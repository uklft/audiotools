from pydub import AudioSegment
from docopt import docopt
import subprocess as sp
import random, sys

def slice(s, a, b, c):
    l = len(s)
    print(f"{l}/{c}*{a} = {l/c*b}")
    return s[l/c*a:l/c*b]

def popen(cmd):
    p = sp.Popen(cmd, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
    out, _ = p.communicate()
    print(out.decode())


def breaksplit(infile, outfile, beats=8, n=1):
    beats = int(beats)
    n = int(n)
    song = AudioSegment.from_wav(infile)
    nb = AudioSegment.empty()
    l = len(song)

    slices = [slice(song, a, a+1, beats) for a in range(0, beats)]
    # slices = list(song[::int(l/beats)])
    for s in slices:
        print(s, len(s))

    for i in range(0,beats*n):
        if i % 4 == 0:
            print(i)
            nb += slices[0]
            continue
        if i % 3 == 0:
            print(i)
            nb += slices[2]
            continue
        # if random.randint(0,10) == 1:
            # nb += slices[0]
            # continue
        # if random.randint(0,10) == 1:
            # nb += slice(slices[0],
        nb += random.choice(slices)
        

    print(len(song)*n)
    print(len(nb))

    nb.export(outfile, format="wav")
    popen(["play", outfile])


if __name__ == '__main__':
    try:
        breaksplit(*sys.argv[1:])
    except:
        breaksplit("amen.wav", "neam.wav", 4, 2)
