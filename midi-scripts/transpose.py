from mididings import *
from subprocess import call
import os


# Define name to look for 
# dev_name = "LPD8"
dev_name = "LPD"
#dev_name = "nanoKEY"
normalize = 1

# Define command strings to find device details
# Use 'aplaymidi', sed and grep to find and filter valid Client Name and Port
dev_port_filter = "aplaymidi -l|grep '%s'|awk '{print $1}'" % dev_name
dev_name_filter = "aplaymidi -l|grep '%s'|awk '{for(i=3;i<=NF;++i)printf $i " "}'|sed -e 's; $;;g'".strip("\n") % dev_name

# Get ouput of actual commands with os.popen
dev_num = os.popen(dev_port_filter).read().strip("\n")
dev_name = os.popen(dev_name_filter).read()


# Check if device port isnt empty, else, exit program
if dev_num is "":
    print "Oh! Device %s does not seem to be connected" % dev_name
    exit
print "Found dev:", dev_num, dev_name


# Call mididings method to configure the interface
config(
  backend='jack', # should also work with jack
#  client_name='LPD8_mididings', # Shown as device name
  client_name=dev_name, # Shown as device name
  in_ports = [('LPD8 MIDI 1',"28:0")],  # For manual setup
  #  in_ports = [(dev_name,dev_num)],  # find with aplaymidi -l
  out_ports = ["trans"] # Number of ports to create. Also takes list of names.
)

normalize_counter = normalize
port_split = []
my_out_port = 1
for key in range(36,55):
    # Catch device signals
        # >> Transpose((36-my_out_port+1)) \
    port_filter = PortFilter(dev_name) \
        >> KeyFilter(key) \
        >> Transpose((36 - normalize_counter + normalize)) \
        >> Output(my_out_port, 1) \
        >> Print(str(key))
    # Append port rule (or patch) to a list
    port_split.append(port_filter)
    print port_filter
    my_out_port += 1
    if normalize:
        normalize_counter += 1




#  port_filter = PortFilter(dev_name) \
                #  >> KeyFilter(key) \
                #  >> Transpose((36 - normalize_counter + normalize)) \
                #  >> Output(out_ports, 1) \
                #  >> Print(str(key)) \
                #  >> Velocity(fixed=127)
run(port_filter)