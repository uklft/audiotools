# Ardour

## Jack, PulseAudio and Ardour
Most Linux distributions come with the software PulseAudio as default sound manager. 
PulseAudio is great when it comes to easy desktop integration or even hosting an audio server with connected clients.
On the other hand, there are limitations built in, such as the latency control.
Thats why i chose Jack to be my Audio driver for production.  

Ardour start its own Jack server by default. 
This is pretty handy as it takes off some extra configuration.
But as Jack and PulseAudio are somehow concurrents on my system, 
Ardour suspends PulseAudio on start and resumes it on exit (*pasuspend*).
This is kind of unfortunate, as all your other desktop applications are configured to use PulseAudio and now cannot play sounds.

To circumvent this, you can either make Jack your default audiosystem,
or pipe all PulseAudio Sound to Jack. 
Thats the way i decided to go, and this is how i've done it:


### Using Jack and PulseAudio together
Install the module  

~~~
sudo apt-get install pulseaudio-module-jack 
~~~

Configure PulseAudio to dump all Output to Jack, if available. 
In `/etc/pulse/default.pa` add:
~~~
load-module module-jack-sink
load-module module-jack-source
~~~

Thats it!
Reboot your system and try it. 
Sometimes you'll need to reconfigure PulseAudio via the Desktop Plugin.


### Second soundcard with Jack
With my current setup i found another problem:
As PulseAudio still is my default audio system, it catches all audio devices on my computer.
So my Guitar Audio Interface is available for PulseAudio, but not in Jack.
Looking into that matter i found `alsa_in, alsa_out`.
This tool creates bridges between alsa devices and Jack.
To route your device, first find the exact name:

~~~
$ aplay -l
**** Liste der Hardware-Geräte (PLAYBACK) ****
Karte 0: HDMI [HDA Intel HDMI], Gerät 3: HDMI 0 [HDMI 0]
Karte 0: HDMI [HDA Intel HDMI], Gerät 7: HDMI 1 [HDMI 1]
Karte 0: HDMI [HDA Intel HDMI], Gerät 8: HDMI 2 [HDMI 2]
Karte 1: PCH [HDA Intel PCH], Gerät 0: ALC3232 Analog [ALC3232 Analog]
Karte 2: Device [C-Media USB Audio Device], Gerät 0: USB Audio [USB Audio]
           |               |
Where    Device Name       |
                         Identifier String
~~~

Now use this information to create the bridge:

~~~
alsa_in -j "C-Media USB Audio Device" -d hw:Device
~~~

This is just a basic setup, the virtual alsa device knows a lot more options for tweaking your channel. 
More info: [here](http://www.penguinproducer.com/Blog/2011/11/using-multiple-devices-with-jack/,
[here](https://github.com/jackaudio/jackaudio.github.com/wiki/WalkThrough_User_AlsaInOut)



## Midi-Madness with Jack, a2j and Ardour
### Pad Controller in Ardour
*How To connect your MIDI controller knobs to Ardour*

* Make sure to check:  `Edit > Control Surfaces > Generic MIDI`
* When using Jack and ALSA the same time, run a2jmidi server to bridge ALSA MIDI and Jack MIDI: `a2jmidid -e &`
* Use QjackCtrl (or some other Jack GUI) to connect your device to the Ardour application
* Ctrl-middleclick the desired automation Fader until the "operate controller now" window pops up
* Operate your controller


### Check the Wires!
When i got deeper into recording MIDI, i thought it must be possible to do this with an e-drumset. Luckily i got one years ago.   

But the first time i connected the USB-B cable to my computer the drumset wasn't recognized, not even with `lsusb`. Looking at my system logs i found something weird:

~~~
hci_hcd
Apr 15 17:29:58 audio kernel: usb 3-3: Device not responding to setup address.
Apr 15 17:29:59 audio kernel: usb 3-3: device descriptor read/8, error -71
Apr 15 17:29:59 audio kernel: usb 3-3: device descriptor read/8, error -71
Apr 15 17:29:59 audio kernel: usb usb3-port3: unable to enumerate USB device
~~~

Googling this finally gave me a hint: There must be something wrong with the currency.  
Now the hacky trick: I simply connected the usb-cable to an USB-Hub, and everything was fine!


### Channel fuck-up
Next Problem: Ardour doesn't trigger the fluidSynth soundfont player. I can see the movement of the incoming signal, but no audio is processed.
My e-drum outputs all events on MIDI Channel __10__, which is the default channel for drum kits.  
And, for some reason, Ardour doesn't map the soundfont to Channel 10!



So MIDIdings to the rescue again:

~~~python
port_filter = 
    PortFilter(dev_name) >>             # Filter devices
    Filter(NOTE|PROGRAM|AFTERTOUCH) >>  # Filter all signals
    Print('in') >>                      # Print signal
    Output(1, 1)>>                      # Output anything on Port 1, Channel 1
    Print('out')                        # Print result
~~~

##### Full patch

~~~
from subprocess import call
from MIDIdings import *
import os

dev_name = "e-drum MIDI 1"
dev_port_filter = "aplayMIDI -l|grep '%s'|awk '{print $1}'" % dev_name
dev_name_filter = "aplayMIDI -l|grep '%s'|awk '{for(i=3;i<=NF;++i)printf $i " "}'|sed -e 's; $;;g'".strip("\n")  % dev_name

dev_num = os.popen(dev_port_filter).read().strip("\n")
dev_name = os.popen(dev_name_filter).read()

config(
  backend='alsa', # should also work with jack
  client_name='E-DRUM', # Shown as device name
  in_ports = [(dev_name,dev_num)],  # find with aplayMIDI -l
  out_ports = ["Channeltrans"] # 1 # Number of ports to create. Also takes list of names.
)

port_filter = PortFilter(dev_name) >> \
              Filter(NOTE|PROGRAM|AFTERTOUCH) >> \
              Print('in') >> \
              Output(1, 1) >> \
              Print('out')
run(port_filter)
~~~
